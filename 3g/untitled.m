signal = [cos(time1*4*pi) cos(time2*8*pi)];

coswave = amplitude1*sin(2*pi*Frequency*Fresolution*Ctime*Tsymbols);
Amp =2;
signal = [Amp*cos(2*pi*4*time1) Amp*cos(2*pi*2*time2)];
plot(signal);

            signal = [cos(time1*4*pi) cos(time2*8*pi)];

coswave = amplitude1*sin(2*pi*Frequency*Fresolution*Ctime*Tsymbols);
Amp =2;
signal = [Amp*cos(2*pi*4*time1) Amp*cos(2*pi*2*time2)];
plot(signal);


time1 = 0:0.025:4;      % from 0 to 4 seconds
time2 = 4.025:0.025:6;  % from 4 to 6 seconds
time = [time1 time2];  % from 0 to 6 seconds

signal = [sin(time1*4*pi) sin(time2*8*pi)];
plot(time,signal)
xlabel('Time');
ylabel('Signal');
title('Signal to be Predicted');

signal = con2seq(signal);

%To set up the problem we will use the first five values of the signal as
%initial input delay states, and the rest for inputs

Xi = signal(1:5);
X = signal(6:end);
timex = time(6:end);

% The targets are now defined to match the inputs. The network is to predict the current input, only using the last five values.
T = signal(6:end);

% The function linearlayer creates a linear layer with a single neuron with a tap delay of the last five inputs.
net = linearlayer(1:5,0.1);
view(net)

% The function *adapt* simulates the network on the input, while
% adjusting its weights and biases after each timestep in response
% to how closely its output matches the target.
% 
% It returns the update networks, it outputs, and its errors.

[net,Y] = adapt(net,X,T,Xi);

figure
plot(timex,cell2mat(Y),timex,cell2mat(T),'+')
xlabel('Time');
ylabel('Output -  Target +');
title('Output and Target Signals');

% The error can also be plotted.
figure
E = cell2mat(T)-cell2mat(Y);
plot(timex,E,'r')
hold off
xlabel('Time');
ylabel('Error');
title('Error Signal');
