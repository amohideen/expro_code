function DPIM = DPIMSymbols2(signalbits, gbs)
global encoded;
GBS = gbs; %default number of guard band slots
%signalbits = message;
DPIM=[];
GapMatrix = [];
parity_hex = [];
msg_hex = [];
msg_length = length(signalbits);
num_symbols = ceil(msg_length/4);
M =5; %modulus
n = 7; % codeword length
k = 4; % message length
% see if the message is of 4 bits else do padding (this should be done at mesage formation)
if  (msg_length/4 == 11)
    
 
%     shaped = reshape(encoded, 7, [])';
      shaped = reshape(signalbits, 4, [])';
      encoded = [];  
    %parity hex
     for row = 1 :1 : length(shaped)
         bits = shaped(row:row,1:4); % 4bits
        enc = encode(bits,n,k,'hamming/binary'); %encode 4 -> 7
   %display(bits);
   %display(enc);
       encoded = [encoded enc];
        decimal = bi2de(enc); % dec of encoded block
   %display(decimal);
       binary = de2bi(decimal,7,'left-msb'); % dec of encoded block
   %display(binary);
         nc = decode(binary,n,k,'hamming/binary'); %encode 4 -> 7
     %     display(nc);
     
        parity_hex = [parity_hex decimal];
        
     end
       
  %display(parity_hex);

%        %sum(A(:) == 1);         
   
   for gap_count = 1:1:length (parity_hex)
        temp=[zeros(1,(parity_hex(gap_count)+GBS))];
     %disp (sum(temp(:) == 0));
        DPIM =[DPIM 1 temp];            
    end         
      
else
    disp('Message is not in multiples of 4bits');
end

  DPIM = [DPIM 1]; % last pulsesubplot(3,1,1);

%  subplot(8,1,1);
%  stem(DPIM);
%  title('Binary Symbols');

