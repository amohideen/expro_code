function Digital_Signal = PulseShaping(dpim)

global Frequency;
global Fresolution; 
global Dtime;
global GB;

clear Digital_Signal;
clear Bask_Signal;
clear bit;
Bask_Signal = []; % Binary Amplitude Shift Keying
Digital_Signal = []; % Timed gray code
DPIM = dpim;

for bit=1:1:length(DPIM) % expansion of DPIM using 100 bits
    if DPIM(bit) == 1;
       se=ones(1,Fresolution);
    else
        DPIM(bit) == 0;
        se=zeros(1,Fresolution);
    end
     Digital_Signal=[Digital_Signal se];
end

digital_time = Frequency:Frequency:length(Digital_Signal)/Fresolution;

subplot(8,1,2);
plot(digital_time,Digital_Signal);
title('Pulse Shaped DPIM Signal');


