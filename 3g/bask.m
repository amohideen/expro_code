%>>>>>>>>> MATLAB code for binary ASK modulation and de-modulation >>>>>>>%
clc;
clear all;
close all;

%% Digital
Tb=1; %bit duration
resolution = 100; %dt frequency
symbol_period = Tb/resolution;

x=[ 1 0 0 1 1 0 1];  % Binary Information
                     
                disp(' Binary information at Trans mitter :');
                disp(x);
                %XX representation of transmitting binary information as digital signal XXX
                bit=[]; 
                for count=1:1:length(x)
                    if x(count)==1;
                       se=ones(1,resolution);
                    else x(count)==0;
                        se=zeros(1,resolution);
                    end
                     bit=[bit se];
                end
                
dt = symbol_period:symbol_period:length(bit)/resolution;
subplot(3,1,1);
plot(dt,bit);
ylabel('amplitude(volt)');
xlabel(' time(sec)');
title('transmitting information as digital signal');


%% XXXXXXXXXXXXXXXXXXXXXXX Binary-ASK modulation XXXXXXXXXXXXXXXXXXXXXXXXXXX%

A1=0.01;                      % Amplitude of carrier signal for information 1
A2=0;                       % Amplitude of carrier signal for information 0
mid =(A1+A2)/2;
Tb = 1;
Cfrequency = 10; % career frequency
bit_period = 1/Cfrequency;
resolution = 100;
sample_period = Tb/resolution;
Ctime = sample_period:sample_period:Tb; %career time

bask_signal=[];
for (i=1:1:length(x))
    if (x(i)==1)
        career=A1*cos(2*pi*Cfrequency*Ctime);
    else
        career=A2*cos(2*pi*Cfrequency*Ctime);
    end
    bask_signal=[bask_signal career]; % BASKed bits
end

noise = A1/2 + (A2-A1/2).*randn(1,length(bask_signal));
bask_signal = bask_signal + noise;
%noise = amplitude1/2 + (amplitude2-amplitude1/2).*randn(1,length(signal));

st=sample_period:sample_period:length(bask_signal)/resolution;
subplot(3,1,2);
plot(st,bask_signal);
xlabel('time(sec)');
ylabel('amplitude(volt)');
title('waveform for binary ASK modulation coresponding binary information');

%% XXXXXXXXXXXXXXXXXXXX Binary ASK demodulation XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
Tb = 1;
Cfrequency = 10; % career frequency
bit_period = 1/Cfrequency;
resolution = 100;
sample_period = Tb/resolution;
Dtime = sample_period:sample_period:Tb; % Demodulator time

demodulated_signal=[];
temp=[];
block=length(Dtime);
career=cos(2*pi*Cfrequency*Dtime); % carrier siignal 

for count=block:block:length(bask_signal) %% for the length of bask signal
  
  correlated_signal=career.*bask_signal((count-(block-1)):count); 
  temp=[temp correlated_signal];
  z=trapz(Dtime,correlated_signal)                       % intregation 
  zz=round((2*z/sample_period))                                     
  
  if(zz>mid)                                 % logic level = (A1+A2)/2=7.5
    a=1;
  else
    a=0;
  end
  demodulated_signal=[demodulated_signal a];
end

disp(' Binary information at Reciver :');
disp(demodulated_signal);

%% XXXXX Representation of binary information as digital signal which achived 
%after ASK demodulation XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
bit=[];
for count=1:length(demodulated_signal)
    if demodulated_signal(count)==1
       se=ones(1,resolution);
    elseif demodulated_signal(count)==0
        se=zeros(1,resolution);
    end
     bit=[bit se];

end

dmt = sample_period:sample_period:length(bit)/resolution;
subplot(3,1,3)
plot(dmt,bit);
ylabel('amplitude(volt)');
xlabel(' time(sec)');
title('recived information as digital signal after binary ASK demodulation');
%>>>>>>>>>>>>>>>>>>>>>>>>>> end of program >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>%