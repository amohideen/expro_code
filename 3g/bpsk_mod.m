clear;
T=1;%Bit rate is assumed to be 1 bit/s;
b=[1 0 1 0 1];
%Rb is the bit rate in bits/second
 
NRZ_out=[];
RZ_out=[];
Manchester_out=[];
  
%Vp is the peak voltage +v of the NRZ waveform
Vp=1;

%Here we encode input bitstream as Bipolar NRZ-L waveform
for index=1:size(b,2)
 if b(index)==1
 NRZ_out=[NRZ_out ones(1,200)*Vp];
 elseif b(index)==0
 NRZ_out=[NRZ_out ones(1,200)*(-Vp)];
 end
end
 
%{Generated bit stream impulses
%figure(1);
%stem(b);
plot(b);
%axis([0 100 -2 2]);
%xlabel('Time (seconds)-->')
%ylabel('Amplitude (volts)-->')
%title('Impulses of bits to be transmitted');
%figure(2);
plot(NRZ_out);
axis([0 length(NRZ_out) -2 2]);
%xlabel('Time (seconds)-->');
%ylabel('Amplitude (volts)-->');
%title('Generated NRZ signal');

t=0.005:0.005:5;
%Frequency of the carrier
f=5;
%Here we generate the modulated signal by multiplying it with 
%carrier (basis function)
Modulated=NRZ_out.*(sqrt(2/T)*cos(2*pi*f*t));
figure;
plot(Modulated);
xlabel('Time (seconds)-->');
ylabel('Amplitude (volts)-->');
title('BPSK Modulated signal');
