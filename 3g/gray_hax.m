function hex_code = gray_hax(hexval)
switch hexval
    case {'0'}
        hex_code = '0';
    case {'1'}
        hex_code = '1';
    case {'2'}
        hex_code = '3';
    case {'3'}
        hex_code = '2';
    case {'4'}
        hex_code = '6';
    case {'5'}
        hex_code = '7';
    case {'6'}
        hex_code = '5';
    case {'7'}
        hex_code = '4';
    case '8'
        hex_code = 'C';
    case '9'
        hex_code = 'D';
    case 'A'
        hex_code = 'F';
    case 'B'
        hex_code = 'E';
    case 'C'
        hex_code = 'A';
    case 'D'
        hex_code = 'B';
    case 'E'
        hex_code = '9';
    case 'F'
        hex_code = '8';
end

 