%
clc

%
 BerTxSymbols_R = table2array(BerTxSymbolsR);
 OutBaskSignal_R = table2array(OutBaskSignalR);
 OutNoisySignal_R = table2array(OutNoisySignalR);
 BerTxSymbols_R(isnan(BerTxSymbols_R))=0;
%
clear BerTxSymbolsR;
clear OutBaskSignalR;
clear OutNoisySignalR;

%
%c1 = 1;
%c100=100; 
noisyPulses = [];
signalPulse =[];
ZeroNoises = [];
PulseZero = [];
[n, m] = size(BerTxSymbols_R);
% Process the raw data
for row= 1:n
    c1 = 1;
    c100=100;
    RoW = BerTxSymbols_R(row,:); % get each row of symbol
    NoisySignalRow = OutNoisySignal_R(row,:); %get each row of noisy signal
    SignalRow = OutBaskSignal_R(row,:);
    disp(row);
    npulse =0;   
    
        for col = 1:1:length(RoW)
                
                if(RoW(col)==1) % it is a pulse
                    %disp(col);
                    %disp('Pulse');
                    
                        if(npulse<11)
                        cut = NoisySignalRow(c1:c100);
                        cut2 = SignalRow(c1:c100);
                        
                        noisyPulses = [noisyPulses; cut];
                        signalPulse = [signalPulse; cut2];
                        c1 = c1+100;
                        c100 = c100 +100;
                        npulse= npulse+1;
                        end
                else
                    %disp('Zero');
                        if(npulse<11)
                        cut = NoisySignalRow(c1:c100); %
                        cut2 = SignalRow(c1:c100);
                        
                        ZeroNoises = [ZeroNoises; cut];
                        PulseZero = [PulseZero; cut2];    
                        c1 = c1+100;
                        c100 = c100 +100;
                        end
                end
                
        end
    
end

%noisyPulses(any(isnan(noisyPulses), 2), :)

save('Data/-2dB_Pulses');
