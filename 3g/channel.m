%channel and noise
freqs = 100;
Frame = 2048;
EbNo = 10;
SNR = 0;
%SNR = EbNo+10*log10(2);
AWGN = comm.AWGNChannel('EbNo', SNR);
ConstScope = comm.ConstellationDiagram;
%Modulator = comm.QPSKModulator('BitInput',true);

hsb = dsp.SpectrumAnalyzer('SampleRate', freqs);
hts = dsp.TimeScope('SampleRate',freqs,'TimeSpan',2);

for i = 1:500
   u = randi([0,1], Frame, 1);
   mod_signal = step(Modulator, u);
   
   rx_signal = step (AWGN,mod_signal);
   
   step(ConstScope, rx_signal);
   
   step (hsb,rx_signal);
   
   step (hts,rx_signal);
end