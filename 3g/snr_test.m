clear;
clc;
close all;
clear global;
%rng default;
global RxSignal;
global t_period;
global sample_period;
global transmit_time;
global receiving_time;
global total_time;
global amplitude1;
global amplitude2;
global decision_boundary;
global symbol_period;
global resolution; 
global Ctime;
global Dtime;
global Cfrequency; % career frequency
global GB;

GB = 2;                             %guard band
amplitude1 = 0.5;
amplitude2 = 0;
decision_boundary = amplitude1/2;
t_period = 1;                  % in seconds
resolution = 100; %dt frequency
Cfrequency = 10; % career frequency
sample_period = t_period/resolution;    %100bps
symbol_period = t_period/resolution;
Ctime = sample_period:sample_period:t_period;
Dtime = sample_period:sample_period:t_period;
transmit_time = 0;
receiving_time = 0;
total_time = 0;
ber = [];
setNodes; % creates nodes and sets parameters

%%
EbSNR = 0:1:10;                % Ec/No range of BER curves
spc = 4;                            % samples per chip

% Preallocate vectors to store BER results:
[berASK868] = deal(zeros(1, length(EbSNR)));

for idx = 1:length(EbSNR) % loop over the EcNo range
  
  message = CreateMessage(62, 62, 0, 0)';  %CreateMessage(link_id, src_dst, data_type, load_type )
  Signal_Bits = DoubleCRC (message);       % along creates the CRC and appends to the message
  preSymbol = Signal_Bits';  
  symbols = DPIMSymbols2(preSymbol, GB);    %breaks the signal into 4 bit 14 blocks 
  Txpulse = PulseShaping(symbols);
  
  waveform = ModulatedSignal(amplitude1, amplitude2, symbols);
   
  K = 1;      % information bits per symbol
  %SNR = EbSNR(idx) - 10*log10(spc) + 10*log10(K);
  receiveds = awgn(waveform, EbSNR(idx));

  [RxSignal Traps TrapsTotal] = node_2.received(receiveds, symbols);
  Rxpulse = PulseShaping(RxSignal);

  [noe berr] = biterr(Txpulse, Rxpulse);
  ber = [ber berr];
  
end

% plot BER curve
figure;
semilogy(EbSNR, ber, '-s');
legend('DPIM, 0.5 Watts','Location', 'southwest')
title('BER Curves')
xlabel('Chip Energy to Noise Spectral Density, Ec/No (dB)')
ylabel('BER')
axis([min(EbSNR) max(EbSNR) 10^-2 1])
grid on
