function dnSignal = denoise(signal) % provides a bandpass signal
global bit_rate;
global t_period;
global bit_period;
global bit_sampling;
global sample_period;
global resolution; 



%time_6=sample_period:sample_period:t_period*length(symbol);
sample_time = sample_period:sample_period:length(signal)/resolution;
%dnSignal = bandpas(signal); %remove noise 
dnSignal = bandpass(signal, [9 11], 100);

subplot(8,1,6);
plot(sample_time, abs(dnSignal));
title('Filtered Signal');
end
 