%% Create 5 nodes and set the parameters
% Node 1 is at the bottom sea level
node_1 = node;
node_1.NodeName = 'Node-1';
node_1.LinkedID = 15;
node_1.NodeAddress = 1;
node_1.receiver = true;
node_1.adjDown = 0;
node_1.NodeDistance = 0;
node_1.NodeType = true;

% node 2-4 are relays
node_2 = node;
node_2.NodeName = 'Node-2';
node_2.LinkedID = 15;
node_2.NodeAddress = 2;
node_2.receiver = true;
node_1.adjUp = 30;
node_2.adjDown = 31;
node_2.NodeDistance = 100;
node_2.NodeType = false;

node_3 = node;
node_3.NodeName = 'Node-3';
node_3.LinkedID = 15;
node_3.NodeAddress = 3;
node_3.receiver = true;
node_2.adjUp = 29;
node_3.adjDown = 30;
node_3.NodeDistance = 200;
node_3.NodeType = false;

node_4 = node;
node_4.NodeName = 'Node-4';
node_4.LinkedID = 15;
node_4.NodeAddress = 4;
node_4.receiver = true;
node_3.adjUp = 28;
node_4.adjDown = 29;
node_4.NodeDistance = 300;
node_4.NodeType = false;

node_5 = node;
node_5.NodeName = 'Node-5';
node_5.LinkedID = 15;
node_5.NodeAddress = 5;
node_5.receiver = true;
node_4.adjUp = 27;
node_5.adjDown = 0;
node_5.NodeDistance = 400;
node_5.NodeType = true;