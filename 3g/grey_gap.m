function gap_value = grey_gap(gray_val)
switch gray_val
    case {'0'}
        gap_value = 0;
    case {'1'}
        gap_value = 1;
    case {'2'}
        gap_value = 2;
    case {'3'}
        gap_value = 3;
    case {'4'}
        gap_value = 4;
    case {'5'}
        gap_value = 5;
    case {'6'}
        gap_value = 6;
    case {'7'}
        gap_value = 7;
    case '8'
        gap_value = 8;
    case '9'
        gap_value = 9;
    case 'A'
        gap_value = 10;
    case 'B'
        gap_value = 11;
    case 'C'
        gap_value = 12;
    case 'D'
        gap_value = 13;
    case 'E'
        gap_value = 14;
    case 'F'
        gap_value = 15;
end

 