%% function symbols = symbols(signalbits)
SignalBits = Signal_Bits;
global bit_period; 
global DPIM; 
bit_period = 1; 
DPIM=[];
GBS=2; %default number of guard band slots

%clear;
close all;
%main;
%T=1;

msg_length = length(SignalBits);
num_symbols = ceil(msg_length/4);
% see if the message is of 4 bits else do padding (this should be done at mesage formation)
if  (msg_length/4 == 14)

    for symbol_count = num_symbols : -1 : 1 % high val: step: low val  
        symbol_matrix(symbol_count,:)= SignalBits(msg_length-3 : msg_length); % 14x4 binary blocks
        symbol_decimal(symbol_count,:) = bi2de(symbol_matrix(symbol_count,:),'left-msb'); % 14x1 bin to decimal
        
        temp_hex = num2str(symbol_matrix(symbol_count,:)); % turn a block of 4 bits from s_m into a binary string
        temp_hex(isspace(temp_hex)) = ''; % remove the gaps in the string
        symbol_hex = bi2hex(temp_hex); %turn the binary string to a hex symbol
         
        gray = hex_grey(symbol_hex); % find the gray_hex for each hex
        gray_coded_hex(symbol_count,:) = gray; % add the gray_hex to vector 
        num_gaps = grey_gap(gray); % find the gap multiplicator for each gray_hex
        
        % inserting number of zeros in DPIM
        temp=[zeros(1,(num_gaps+GBS))];
        % inserting '1' at the start of each symbol
        DPIM =[DPIM 1 temp];
       
        % disp equivalent values
%       disp (symbol_hex);
%       disp (gray);
%       disp(num_gaps);
%       disp('---');
%       disp (temp)
        msg_length = msg_length - 4;
    end
    else
    disp('Message is not in multiples of 4bits');
end

DPIM = [DPIM 1]; % last pulse
%plot(DPIM);
%stem(DPIM)
%-----------------------------------------------------

%% --------------------------------
Bask_Signal = []; % Binary Amplitude Shift Keying
Digital_Signal = []; % Timed gray code

for bit=1:1:length(DPIM) % expansion of DPIM using 100 bits
    if DPIM(bit)==1;
       se=ones(1,100);
    else DPIM(bit)==0;
        se=zeros(1,100);
    end
     Digital_Signal=[Digital_Signal se];
end

time_1=bit_period/100:bit_period/100:100*length(DPIM)*(bit_period/100);
% subplot(3,1,1);
% plot(time_1,DPIM(bit),'lineWidth',2.5);grid on;
% axis([ 0 bit_period*length(DPIM) -.5 1.5]);
% ylabel('amplitude(volt)');
% xlabel(' time(sec)');
% title('transmitting information as digital signal');

%% career modulation
Amplitude1=1;                      % Amplitude of carrier signal for information 1
Amplitude2=0;                       % Amplitude of carrier signal for information 0
bit_rate=1/bit_period;                                                         % bit rate
career_frequency=bit_rate*50; %(10MHz)                                       % carrier frequency 

time_2=bit_period/99:bit_period/99:bit_period;

ss=length(time_2);

%Bask_Signal=[];
for count=1:1:length(DPIM)
    if (DPIM(count)==1)
        bask=Amplitude1*cos(2*pi*career_frequency*time_2);
    else
        bask=Amplitude2*cos(2*pi*career_frequency*time_2);
    end
    Bask_Signal=[Bask_Signal bask]; % BASKed bits
end

time_3=bit_period/99:bit_period/99:bit_period*length(DPIM);
%  subplot(3,1,2);
%  plot(time_3,Bask_Signal);
%  xlabel('time(sec)');
%  ylabel('amplitude(volt)');
%  title('waveform for binary ASK modulation coresponding binary information');

% %XXXXXXXXXXXXXXXXXXXX Binary ASK demodulation XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
% demod_dpim=[];
% %t2=bp/99:bp/99:bp;
% %ss=length(t2);
% %ss is 99
% for num=ss:ss:length(Bask_Signal) %% for the length of bask signal
%   t=bit_period/99:bit_period/99:bit_period;
%   career=cos(2*pi*career_frequency*t); % carrier siignal 
%   
%   mm=career.*Bask_Signal((num-(ss-1)):num); %%%%%%%%%%%%%%%%%%%%%%
%   
%   time_4=bit_period/99:bit_period/99:bit_period;
%   z=trapz(time_4,mm)                                              % intregation 
%   zz=round((2*z/bit_period))                                     
%   if(zz>5)                                  % logic level = (A1+A2)/2=7.5
%     a=1;
%   else
%     a=0;
%   end
%   demod_dpim=[demod_dpim a];
% end
% 
% if (DPIM == demod_dpim)
%     disp(' Binary information at Reciver Matches');
% else
%         disp(' Binary information at Reciver does not Match');
% end
% %disp(demod_dpim);
% 
% 
