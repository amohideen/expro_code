
function gray = hex_gray(x)
hex=x;
for i=1:length(hex)
    if hex(i)=='F'
        gray((i*4)-3:i*4)=[1 1 1 1];
    elseif hex(i)=='E'
        gray((i*4)-3:i*4)=[1 1 1 0];
    elseif hex(i)=='D'
        gray((i*4)-3:i*4)=[1 1 0 1];
    elseif hex(i)=='C'
        gray((i*4)-3:i*4)=[1 1 0 0];
    elseif hex(i)=='B'
        gray((i*4)-3:i*4)=[1 0 1 1];
    elseif hex(i)=='A'
        gray((i*4)-3:i*4)=[1 0 1 0];
    elseif hex(i)=='9'
        gray((i*4)-3:i*4)=[1 0 0 1];
    elseif hex(i)=='8'
        gray((i*4)-3:i*4)=[1 0 0 0];
    elseif hex(i)=='7'
        gray((i*4)-3:i*4)=[0 1 1 1];
    elseif hex(i)=='6'
        gray((i*4)-3:i*4)=[0 1 1 0];
    elseif hex(i)=='5'
        gray((i*4)-3:i*4)=[0 1 0 1];
    elseif hex(i)=='4'
        gray((i*4)-3:i*4)=[0 1 0 0];
    elseif hex(i)=='3'
        gray((i*4)-3:i*4)=[0 0 1 1];
    elseif hex(i)=='2'
        gray((i*4)-3:i*4)=[0 0 1 0];
    elseif hex(i)=='1'
        gray((i*4)-3:i*4)=[0 0 0 1];
    elseif hex(i)=='0'
        gray((i*4)-3:i*4)=[0 0 0 0];
    end
end  
