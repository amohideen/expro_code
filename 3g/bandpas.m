function fSignal = bandpas(signal,frequency_career)
global bit_rate;
global t_period;
t_period = 0.000001;
bit_sampling = bit_rate -1;

br=1/t_period;                                                         % bit rate
frequency=br*frequency_career;    


filter_order = 1;
cut_off = [5 6]*2/frequency;
[b a] = butter(filter_order, cut_off, 'bandpass');
fSignal = filter(b,a, signal);
end
 