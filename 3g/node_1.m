classdef node < handle
    %UNTITLED5 Summary of this class goes here
    %   Detailed explanation goes here
    %%
    properties (SetAccess = public)
        NodeName = ''       
        NodeAddress = zeros(5,1);   % 5 binary bits
        LinkedID = zeros(6,1);   % 6 binary bits
        transmitter = false(1);
        receiver = false(1);
        adjUp = zeros(5,1);
        adjDown = zeros(5,1);
        NodeDistance = 0;
        NodeType = false(1); % 0 = relay 1 = end-node
    end
    %%
    events 
        Txing;
        Rxing;
    end
    %%
    methods
        %% set the Node Name
        function set.NodeName(node,nodnm)
             node.NodeName = nodnm;
             fprintf('Node %s is Created : \n',nodnm);
        end
        %
        %%   set the LinkID
        function set.LinkedID(node,lnkid)
            if (lnkid == 63 || lnkid<63)
                node.LinkedID = de2bi(lnkid);
                link = de2bi(lnkid);
                %disp('Address set');
                fprintf('Link ID is set to : \n');
                disp(link);
            else
                disp('The value MUST be 63 or below 63');
            end
        end
        
        %% set the SrcDst
        function set.NodeAddress(node,adrs)
            %   set the source or destination address
            if (adrs == 31 || adrs<31)
                node.NodeAddress = de2bi(adrs);
                address = de2bi(adrs);
                %disp('Address set');
                fprintf('Address is set to : %s \n', transpose(address));
                disp(address);
            else
                disp('The value MUST be 31 or below 31');
            end
        end
          %%
        function set.transmitter(node,node_state)
            state = node_state;
            if (state == 1)
                prnt = 'True';
                nodestate = true;
                node.transmitter = nodestate;
                %name = node.NodeName;
                fprintf('Node is set to Transmitting :%s \n', prnt);
             elseif (state == 0)
                 prnt = 'False';
                 nodestate = false;
                 node.transmitter = nodestate;
                fprintf('Node is set to transmitting :%s \n',prnt);
            end
        end
        %
         %%
        function set.receiver(node,node_state)
            state = node_state;
            if (state == 1)
                prnt = 'True';
                nodestate = true;
                node.receiver = nodestate;
                fprintf('Node is set to Receiving :%s \n',prnt);
            elseif (state == 0)
                 prnt = 'False';
                 nodestate = false;
                 node.receiver = nodestate;
                fprintf('Node is set to receiving :%s \n',prnt);
            end
        end
        
        %% set adjecsent Node Up/Down
        function set.adjUp(node,adrs)
             if (adrs == 31 || adrs<31)
                node.adjUp = de2bi(adrs);
                address = de2bi(adrs);
                %disp('Address set');
                fprintf('Adjecsent (UP) node is set to : %s \n', transpose(address));
                disp(address);
             elseif (adrs == 0 ) % for surface device
                node.adjUp = de2bi(adrs);
                address = de2bi(adrs);
                %disp('Address set');
                fprintf('Adjecsent (UP) node is set to : %s \n', transpose(address));
                disp(address);
            end
        end
        %
    
         %% set adjecsent Node Up/Down
        function set.adjDown(node,adrs)
             if (adrs == 31 || adrs<31)
                node.adjDown = de2bi(adrs);
                address = de2bi(adrs);
                %disp('Address set');
                fprintf('Adjecsent (UP) node is set to : %s \n', transpose(address));
                disp(address);
             elseif (adrs == 0 ) % for under sea device
                node.adjDown = de2bi(adrs);
                address = de2bi(adrs);
                %disp('Address set');
                fprintf('Adjecsent (UP) node is set to : %s \n', transpose(address));
                disp(address);
            end
        end
        %
        %% set the Node Name
        function set.NodeDistance(node,nodis)
             node.NodeDistance = nodis;
             fprintf('Node %s is : \n',nodis);
        end
        %
        
         %%
        function set.NodeType(node,node_type)
            type = node_type;
            if (type == 1)
                prnt = 'End Node';
                nodetype = true;
                node.NodeType = nodetype;
                %name = node.NodeName;
                fprintf('Node is set to :%s \n', prnt);
             elseif (type == 0)
                 prnt = 'Relay';
                 nodetype = false;
                 node.NodeType = nodetype;
                fprintf('Node is set to :%s \n',prnt);
            end
        end
        %
    %%
%     methods (Access = public)
    
        function TxSignal = transmission(nodes,node_state, snr_noise, signal, symbol)
            state = node_state;
            global RxSignal;
            global rx_signal;
            
            if (state == 1)
                nodes.transmitter=true;
                nodes.receiver=false;
                node_name = nodes.NodeName;
                disp('--------------------------------');
                fprintf('%s is set to Transmitting : \n',node_name);
                disp('Setting up Transmitter [8 secons..]');
                pause (8);
                
                SNR = snr_noise;
                freqs = 100;
                EbNo = 0.01;
                signal_tx = signal;
                SNR = EbNo+10*log10(2);
               
                AWGN = comm.AWGNChannel('EbNo',EbNo); %AWGN channel
                  %channelOutput = AWGN(signal_tx);
                  %scatterplot(signal_tx);
                  %scatterplot(channelOutput);
                 
                 SpectrumScope = dsp.SpectrumAnalyzer('SampleRate', freqs);
                 TimeScope = dsp.TimeScope('SampleRate',freqs,'TimeSpan',120);
                 disp('Transmitting now...');
                 
                                  
                 for tx = 1:1:length(signal_tx)
                    %pause(0.0101);
                    
                    rx_signal = step(AWGN,signal_tx(tx));
                    RxSignal = [RxSignal rx_signal];
                    TxSignal = RxSignal;
                    
                    step (SpectrumScope,rx_signal);
                    step (TimeScope,rx_signal);
                                      
                 end
                 % notify at the end of transmission as the other node
                 % needs to wait out this period
                 
                 release(AWGN);
                 nodes.transmitter=0;
                 nodes.receiver=1;
                 %--------------------------------------
                    global t_period;
                %global bit_period;
                global bit_rate;
                bit_sampling = bit_rate -1;
                
                time_5=t_period/bit_sampling:t_period/bit_sampling:t_period*length(symbol);
                subplot(5,1,5);
                plot(time_5, TxSignal);
                %plot (real(addNoise),imag(addNoise), '*');
                title('Received Signal with Noise (SNR)');

                 % put everybody else on listen
            else
                % implement receiving
                disp (nodes.transmitter);
            end            
        end
        %-------------------
        function notifier(obj)
           notify(obj, 'Txing'); 
        end
        %%
        function addNoise = transmito(nodes,node_state, snr_noise, signal, symbol)
                        
            nodes.transmitter=true;
            nodes.receiver=false;
            node_name = nodes.NodeName;
            global transmit_time;
            global t_period;
            
            disp('--------------------------------');
            fprintf('%s is set to Transmitting : \n',node_name);
            disp('Setting up Transmitter [8 secons..]');
            pause (8);
            transmit_time = transmit_time + 8;
            transmit_time = transmit_time + ((length(symbol))* t_period);
            
%             snr = snr_noise;           
%             p_signal = mean(abs(signal).^2); % to see signal power
%             e_signal = (abs(signal).^2); % signal energy or energy/bit
%             
%             snr_lin = 10^(snr/10); %Signal/Noise = SNR-Linear
%             var = (p_signal/snr_lin);
%             noice = 1/sqrt(2)*(randn(1,length(signal))+i*randn(1,length(signal)))*var;  %noise based on SNR
%             %noise based on EbNo
%             addNoise = signal+noice;
              r = -20 + (20+20)*rand(1,length(signal));
              addNoise = signal + r;

            
            %scope = dsp.SpectrumAnalyzer('SampleRate',100);
            SpectrumScope = dsp.SpectrumAnalyzer('SampleRate', 100,...
                                                'Position', [20 10 800 450],...
                                                'YLimits', [-50,50]);
                                
            TimeScope = dsp.TimeScope('SampleRate',100,'TimeSpan',180,...
                                                'Position', [850 10 600 350]);
            
            for tx = 1:1:length(addNoise)
                    SpectrumScope (addNoise(tx));
            end
            
             for tx = 1:1:length(addNoise)
                    TimeScope (addNoise(tx));
                    %pause (0.01);
             end
            
            nodes.transmitter=0;
            nodes.receiver=1;
    
            %global t_period;
            %global bit_period;
            global bit_rate;
            bit_sampling = bit_rate -1;
            %time_2=t_period/bit_sampling:t_period/bit_sampling:t_period;
            %time_5=t_period/99:t_period/99:t_period*length(addNoise);
            time_5=t_period/bit_sampling:t_period/bit_sampling:t_period*length(symbol);
            subplot(5,1,5);
            plot(time_5, real(addNoise));
            %plot (real(addNoise),imag(addNoise), '*');
            title('Received Signal with Noise (SNR)');
            fprintf('Time taken to transmit: %d seconds\n', transmit_time);
        end    
        %% implement the demodulation in here 
        function rx_received = reception(nodes, signal, dpim,frequency_career)
            global decision_boundary;
            global t_period;
             %global bit_period;
            global bit_rate;
            bit_sampling = bit_rate -1;
            cfs = frequency_career;

            BASK_Signal = signal;
            bit_period = t_period;
            DPIM = dpim;
            bit_rate=1/bit_period; % bit rate
            career_frequency=bit_rate*cfs; %(10MHz)    
            tesnode = nodes;
            disp('--------------------------'); 
            disp('Starting to Receive Tx Signal');
           %XXXXXXXXXXXXXXXXXX Binary ASK demodulation XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
            demod_dpim=[];
            time_2=bit_period/bit_sampling:bit_period/bit_sampling:bit_period;
            ss=length(time_2);
            
            time=bit_period/bit_sampling:bit_period/bit_sampling:bit_period;
            career=cos(2*pi*career_frequency*time); % carrier signal 
            time_4=bit_period/99:bit_period/99:bit_period;
            
            for num=ss:ss:length(BASK_Signal) %% for the length of bask signal
                mm=career.*BASK_Signal((num-(ss-1)):num); %%%%%%%%%%%%%%%%%%%%%%             
                z=trapz(time_4,mm); %Trapezoidal numerical integration 
                zz=round((2*z/bit_period));                                     
                if(zz>decision_boundary)  % Half of amplitude logic = (A1+A2)/2=7.5
                    a=1;
                else
                    a=0;
                end
                demod_dpim=[demod_dpim a];
            end
            rx_received = demod_dpim;

            if (DPIM == demod_dpim)
                disp('Symbol information at Reciver Matches with Transmitted Signal');
            else
                disp('Symbol information at Reciver does not Match');
            end
            %disp(demod_dpim);
            nodes.decrypt(demod_dpim);
            %----------
           
        end
%%
        
        function binary_signal = decrypt(nodes, dpim) % returns the decrypted binary signal
            signal = dpim;
            code = 0;
            graycode = [];
            global binary_signal; 
            binary_signal=[];
    
            RLE = diff([0 find(diff(dpim)~=0) length(dpim)]);
            
            for gray_count = 1:1:length(RLE)
                
                if (RLE (gray_count)~=1)
                code = RLE(gray_count)-2; %2GBS
                %disp ((code));
                graycode = [graycode code]; % gaps representing the gray
                end             
            end
        
            for bin_count = 1:1:length(graycode)
                hexcode = g2h(graycode(bin_count));
                binary_signal = [binary_signal h2b(hexcode)];
                %disp (binary_signal);
            end
            
        end

 %% implement the demodulation in here 
        function demod_dpim = received(nodes, signal,frequency_career,dpim)
            global decision_boundary;
            global t_period;
            global bit_rate;
            global transmit_time;
            global processing_time;
            processing_time = 9;
            bit_sampling = bit_rate -1;
            time_2=t_period/bit_sampling:t_period/bit_sampling:t_period;

            cfs = frequency_career;
            career_frequency=bit_rate*cfs; %(10MHz) carrier frequency 
            BASK_Signal = signal;
            DPIM = dpim;
            tesnode = nodes;
            %disp('--------------------------'); 
            %disp('Starting to Receive Signal');
            fprintf('\n');
            fprintf('%s: Starting to Receive Signal\n', nodes.NodeName);
            
            demod_dpim = [];
            ss = length(time_2);
            time = t_period/bit_sampling:t_period/bit_sampling:t_period;
            career = cos(2*pi*career_frequency*time); % carrier signal 
            time_4 = t_period/bit_sampling:t_period/bit_sampling:t_period;
            
            for num = ss:ss:length(BASK_Signal) %% for the length of bask signal
                mm = career.*BASK_Signal((num-(ss-1)):num); %%%%%%%%%%%%%%%%%%%%%%
                z = trapz(time_4,mm);   % %Trapezoidal numerical integration  
                zz = round((2*z/t_period));                                     
                if(zz> decision_boundary)  % Half of amplitude  
                                           % logic level = (A1+A2)/2=7.5
                    a = 1;
                else
                    a = 0;
                end
                demod_dpim = [demod_dpim a];
            end
            fprintf('%s: Starting to process Signal\n', nodes.NodeName);
            fprintf('Signal processing time: %d seconds\n',processing_time);
            pause (processing_time);
            %disp (length(DPIM));

            if (DPIM == demod_dpim)
                disp('Symbol information at Reciver Matches');
            else
                disp('Symbol information at Reciver does not Match');
            end
            %disp(demod_dpim);
            nodes.decrypt(demod_dpim);
            %----------
           
           transmit_time = (transmit_time + processing_time);
           fprintf('Time taken to tranceive: %d seconds\n',transmit_time);
        end
              
        %% -------------------
        
        function SrcDst = GetAddress(nodes, binary_signal)
            SrcDst = [];
            for index=7:1:11
            SrcDst = [SrcDst binary_signal(index)];
            end
            %disp(SrcDst);               
        end
        %%
        function ReTx = IsForMe(nodes, address)
            if nodes.NodeAddress == address
            fprintf('%s: This message is for me \n', nodes.NodeName);
            ReTx = false;
            else
            fprintf('%s: This message is NOT for me \n', nodes.NodeName);
            ReTx = true;
            end
        end    
             
    end % end methods
end

