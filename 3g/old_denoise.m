function dnSignal = denoise(signal, symbol) % provides a bandpass signal
global bit_rate;
global t_period;
global sample_frequency;
sample_frequency =100;

bit_sampling = bit_rate -1;
time_6=t_period/bit_sampling:t_period/bit_sampling:t_period*length(symbol);

%dnSignal = bandpas(signal); %remove noise 
dnSignal = bandpass(signal, [8 12], sample_frequency);

subplot(8,1,6);
plot(time_6,dnSignal);
title('Filtered Signal');
end
 