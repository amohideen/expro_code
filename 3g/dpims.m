             
function DPIM = dpims(M,nsym,NGS)
% function to generate DPIM sequence
% M: bit resolution
% nsym: number of symbols
% NGS: number of guard slots (default value is zero)

if nargin == 2
NGS=0; %default number of guard slots
end

DPIM=[];

for i= 1:nsym
%inpb(i,:)= randint(1,M); -randint is obsolete therefore dandi is used  
% creates nsym by M matrix of 1s and 0s
inpb(i,:)=randi([0 1],1,M);
end

for i=1:nsym
% Converting Ith row of the matrix (inpb) from binary to decimal number
inpd=bi2de(inpb(i,:),'left-msb');
% inserting number of zeros in DPIM
temp=[zeros(1,(inpd+NGS))];
% temp is a 1xdec of (inpb[i]), if dec value is 9 then it is 1x9 vec of 0s
% inserting '1' at the start of each symbol
DPIM =[DPIM 1 temp];
end
end
    
