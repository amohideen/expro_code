function binary = h2b(h)
switch h
    case {0}
        binary = [0 0 0 0];
    case {1}
        binary = [0 0 0 1];
    case {2}
        binary = [0 0 1 0];
    case {3}
        binary = [0 0 1 1];
    case {4}
        binary = [0 1 0 0];
    case {5}
        binary = [0 1 0 1];
    case {6}
        binary = [0 1 1 0];
    case {7}
        binary = [0 1 1 1];
    case {8}
        binary = [1 0 0 0];
    case {9}
        binary = [1 0 0 1];
    %case {A, a}
    case {10}
        binary = [1 0 1 0];
    %case {B, b}
    case {11}
        binary = [1 0 1 1];
    %case {C, c}
    case {12}
        binary = [1 1 0 0];
    %case {D, d}
    case {13}
        binary = [1 1 0 1];
    case {14}
    %case {E, e}
        binary = [1 1 1 0];
    case {15}
    %case {F, f}
        binary = [1 1 1 1];
end