function Bask_Signal = ModulatedSignal(amplitude1, amplitude2, dpim)
%% career modulation 

global period;
global sample_period;
global Frequency;
global symbol_samples;

%global symbol_period;
global Fresolution; 
global Ctime;
global Tsymbols; % Symbol period 

Bask_Signal=[];
DPIM = dpim;

%Ctime = sample_period:sample_period:t_period;
Ctime = Frequency:Frequency:period;

for count=1:1:length(DPIM)
    
    if (DPIM(count)==1)
        %disp (DPIM(count));
        bask = amplitude1*sin(2*pi*Frequency*Fresolution*Ctime*Tsymbols);
    else
        bask = amplitude2*sin(2*pi*Frequency*Fresolution*Ctime*Tsymbols);
    end
    Bask_Signal=[Bask_Signal bask]; % BASKed bits
    
    coswave = amplitude1*sin(2*pi*Frequency*Fresolution*Ctime*Tsymbols);
    %CosWave = [CosWave coswave];
end
    %Bask_Signal = Bask_Signal + amplitude1*cos(2*pi*(frequency*2)*time_3);
    
    sample_time = Frequency:Frequency:length(Bask_Signal)/Fresolution;
    cos_time = Frequency:Frequency:Ctime*Tsymbols; 
   
        
%     subplot(8,1,4);
%     plot(sample_time,Bask_Signal);
%     xlabel('time(sec)');
%     ylabel('amplitude(volt)');
%     title('Bandpass Modulated DPIM');
% 
%     subplot(8,1,3);
%     plot(Ctime,coswave);
%     xlabel('time(sec)');
%     ylabel('amplitude(volt)');
%     title('Career signal for the total symbol time) ');
end

