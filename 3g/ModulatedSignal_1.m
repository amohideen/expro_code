function Bask_Signal = ModulatedSignal(amplitude1, amplitude2, frequency_career, dpim)
%% career modulation at 100bps
global t_period;
global bit_period;
global bit_rate;

samples_per_second = 100;   %samples per second
time_per_sample = 1/samples_per_second;          %seconds per sample
stop_time = 1;
%time_2 = (0:time_per_sample:stop_time);

t_period = 1; % in seconds
bit_period = t_period/100; %100bps
bit_rate = 1/bit_period;% 100bps bit rate

% global t_period;
% %global bit_period;
% global bit_rate;
bit_sampling = bit_rate -1;
time_2=t_period/bit_sampling:t_period/bit_sampling:t_period;

Amplitude1 = amplitude1; % Amplitude of carrier signal for information 1
Amplitude2 = amplitude2; % Amplitude of carrier signal for information 0
cfs = frequency_career;
career_frequency=bit_rate*cfs; %(10MHz) carrier frequency 
%career_frequency=frequency_career;

signal_i = dpim;
signal_q = zeros(1,length(signal_i));
DPIM = complex(signal_i,signal_q);

ss=length(time_2);
Bask_Signal=[];
time_3=t_period/bit_sampling:t_period/bit_sampling:t_period*length(DPIM);

%time_t=time_per_sample:time_per_sample:t_period*length(DPIM);

for count=1:1:length(DPIM)
    
    if (DPIM(count)==1)
        %disp (DPIM(count));
        bask=Amplitude1*cos(2*pi*career_frequency*time_2);
    else
        bask=Amplitude2*cos(2*pi*career_frequency*time_2);
    end
    Bask_Signal=[Bask_Signal bask]; % BASKed bits
    coswave = Amplitude1*cos(2*pi*career_frequency*time_3);
end

%time_3=bit_period/bit_sampling:bit_period/bit_sampling:bit_period*length(DPIM);

%time_3=bit_period/99:bit_period/99:bit_period*length(DPIM);
    
    subplot(5,1,4);
    plot(time_3,Bask_Signal);
    xlabel('time(sec)');
    ylabel('amplitude(volt)');
    title('waveform for binary ASK modulation coresponding binary information');

    subplot(5,1,3);
    plot(time_3,coswave);
    xlabel('time(sec)');
    ylabel('amplitude(volt)');
    title('waveform for career signal - Amplitude*cos(2*pi*CFS*time) ');
end

