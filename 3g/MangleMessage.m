function Signal_Bits = MangleMessage(address, signal)
clear MessageBits;
clear Signal_Bits;

MessageBits = [];
count = 1;
for index = 1:1:49
 MessageBits = [MessageBits signal(index)]; 
end
MessageBits = MessageBits';
% for index=7:1:11
%     MessageBits (index) = address(count);
%     count =  count+1;
% end

 CrcGen = comm.CRCGenerator('x^7+ x^4+ x^3+ x^1+ x^0');
 reset(CrcGen); 

Signal_Bits = CrcGen(MessageBits);
%Signal_Bits = MessageBits;
%disp (Signal_Bits);
end

