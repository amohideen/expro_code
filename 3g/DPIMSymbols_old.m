function DPIM = DPIMSymbols2(signalbits, gbs)
SignalBits = signalbits;
GBS = gbs; %default number of guard band slots
DPIM=[];
GapMatrix = [];
msg_length = length(SignalBits);
num_symbols = ceil(msg_length/4);
% see if the message is of 4 bits else do padding (this should be done at mesage formation)
if  (msg_length/4 == 12)

    for symbol_count = num_symbols : -1 : 1 % high val: step: low val  
        symbol_matrix(symbol_count,:)= SignalBits(msg_length-3 : msg_length); % 12x4 binary blocks
        disp (symbol_matrix);
        
        %implement gray and encode;
        % bin to hex, hex to gray;
        
        symbol_decimal(symbol_count,:) = bi2de(symbol_matrix(symbol_count,:),'left-msb'); % 14x1 bin to decimal
        
        temp_hex = num2str(symbol_matrix(symbol_count,:)); % turn a block of 4 bits from s_m into a binary string
        temp_hex(isspace(temp_hex)) = ''; % remove the gaps in the string
        symbol_hex = bi2hex(temp_hex); %turn the binary string to a hex symbol
         
        gray = hex_grey(symbol_hex); % find the gray_hex for each hex
        gray_coded_hex(symbol_count,:) = gray; % add the gray_hex to vector 
        num_gaps = grey_gap(gray); % find the gap multiplicator for each gray_hex
        GapMatrix (symbol_count) = num_gaps; 
        % inserting number of zeros in DPIM
%        temp=[zeros(1,(num_gaps+GBS))];
        % inserting '1' at the start of each symbol
%        DPIM =[DPIM 1 temp];
        %DPIM =[1 temp DPIM];
        % disp equivalent values
%       disp (symbol_hex);
%       disp (gray);
%       disp(num_gaps);
%       disp('---');
%       disp (temp)
        %disp (GapMatrix (symbol_count));
        msg_length = msg_length - 4;
    end
    %disp ('----------------------------');
    
    for gap_count = 1:1:length (GapMatrix)
        temp=[zeros(1,(GapMatrix(gap_count)+GBS))];
      %  disp (GapMatrix(gap_count));
        DPIM =[DPIM 1 temp];            
    end    
    
else
    disp('Message is not in multiples of 4bits');
end

DPIM = [DPIM 1]; % last pulsesubplot(3,1,1);
 subplot(8,1,1);
 stem(DPIM);
 title('Binary Symbols');

 
 
for row = 1 : 3
  for col = 1 : 50
    ca{row, col} = rand(1, 4);
  end
end