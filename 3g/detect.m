% DtSignal=[];
% Dt_Signal = [];
% ber = [];
% ber_theory = [];
% ber_qfunc =[];
% 
% for lot = 1:1:length(RxSignal)
%     if (RxSignal(lot)> 2.5)
%         DtSignal = [DtSignal 1];
%     else
%         DtSignal = [DtSignal 0];
%     end
%     
% end
%XXXXXXXXXXXXXXXXXXXX Binary ASK demodulation XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
mn=[];
%t2=bp/99:bp/99:bp;
%ss=length(t2);
sampling_frequency = 100; % Sampling rate - This will define the resoultion/must match the channel for timing
bit_time = 0: 1/sampling_frequency : 1; % Time window for one bit sampling
ss = 99;

for count=1:ss:length(RxSignal)
  %t=bp/99:bp/99:bp;
  %y=cos(2*pi*f*t); % carrier siignal 
  y=sin(2*pi*20*bit_time);
  
  mm=y.*RxSignal((count-(ss-1)):count); %%%%%%%%%%%%%%%%%%%%%%
  
  %t4=bp/99:bp/99:bp;
  z=trapz(bit_time,mm)                                              % intregation 
  zz=round((2*z/bit_time))                                     
  if(zz>2.5)                                  % logic level = (A1+A2)/2=7.5
    a=1;
  else
    a=0;
  end
  mn=[mn a];
end
disp(' Binary information at Reciver :');
disp(mn);