clc
clear
clear global;

%   delete BerMessageBits.dat;
%   delete BerTxSymbols.dat;
% % % delete BerBaskSignal.dat;
% % % delete BerNoisySignal.dat;
   delete OutBaskSignal_R.dat;
   delete OutNoisySignal_R.dat;
   delete OutMessageBits_R.dat
   delete BerTxSymbols_R.dat;
   delete BerVal_R.dat;
%  
% % delete TxTime.dat;
% % delete RxTime.dat;
% % delete TRxTime.dat;

BerRunIndex = 10;
BerEcNO = [];
BerIndex = [];
BerBASK = [];
BerNo = -10:1:0; % Ec/No range of BER curves
%[BerBASK] = deal(zeros(1, length(BerNo)));
Berspc = 1;
BerK = 1;      % information bits per symbol
AskSignal = [];

TransmitTime = [];
ReceiveTime =[];
TexLength= [];
%Power
TxEnergy= [];
RxEnergy =[];
Energy =[];
Tperiod = 1;
NoPulse =12;
Pt=100;%100W
Pidle=0.04; %40mW
SetupTime = 8;


for BerEcNoIndex = 1:length(BerNo)  %for each EcNo

    for indx = 1:1:BerRunIndex % Monte-carlo range
        
        SnR = BerNo(BerEcNoIndex) - 10*log10(Berspc) + 10*log10(BerK); % same SNR for RunIndex
        %SnR = 0;
        BerEcNO = [BerEcNO SnR];
        BerIndex = [BerIndex BerNo(BerEcNoIndex)];
        disp (SnR);
        snr_noise = SnR;
        main;        
        BerBASK = [BerBASK ber];
        %X = reshape(BerIndex,2,[])' to get the the total of all
        %X = reshape(BerEcNO,2,[])' ***replace 2 with BerRunIndex
    end

end

BerEnoReshaped = reshape(BerEcNO,BerRunIndex, [])';
BerIndexReshaped = reshape(BerIndex,BerRunIndex, [])';
BerBASKReshaped = reshape(BerBASK,BerRunIndex, [])';
BerEnergy = reshape(Energy,BerRunIndex, [])';
BerTexLength = reshape(TexLength, BerRunIndex, [])';
BerTransTime = reshape(TransmitTime,BerRunIndex, [])';

% OutBasicSignal = dlmread('OutBaskSignal_R.dat');
% OutNoisySignal = dlmread('OutNoisySignal_R.dat');


%BerLabel  = BerEcNO';

%dlmwrite('BerLabel.dat', BerLabel, '-append','delimiter', '\t');

% BerMessageBits = dlmread('BerMessageBits.dat');
% BerTxSymbolBits = dlmread('BerTxSymbols.dat');
% BerBaskSignal = textread('BerBaskSignal.dat');
% BerNoisySignal = dlmread('BerNoisySignal.dat');
% BerRxSymbols = textread('BerRxSymbols.dat');
% BerVal = dlmread('BerVal.dat');
%dlmwrite(file_name,oldfile,A,'-append','newline','pc','delimiter',' ')
%A = importdata('output.polygon',' ');

%A1=table2array(OutBaskSignal_R);
%TxSymbols = table2array(BerTxSymbols_R); %after loadibg from the import
%tool

% fid = fopen ('BerNoisySignal.dat','r'); 
% ii  = 1;
% while ~feof(fid)
%     %acorrente(ii, : ) = str2num(fgets(fid));
%     acorrente(ii,: ) = str2num(fgets(fid));
%     disp(acorrente);
%     ii = ii + 1;
% end
% 
% 
% BerNoisySignal = dlmread('BerNoisySignal.dat');

%clear;
