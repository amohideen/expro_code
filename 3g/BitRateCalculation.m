clear all;
clear;
clc;
close all;

Bit_Period = 1/600;
Bit_Rate = 1/Bit_Period;

Time_for_bit = Bit_Period/99: Bit_Period/99 :Bit_Period;  % 100 samples
%Time_for_bit = Bit_Period/50: Bit_Period/50 :Bit_Period; % 50 samples 
Amplitude = 10;             % amplitude
Career_Frequence = Bit_Rate * 10;  % career frequency %125KHz 125000/600 = 208
Tx_10 = Amplitude * cos (2*pi*Career_Frequence*Time_for_bit);
Tx_208 = Amplitude * cos (2*pi*Career_Frequence*Time_for_bit);
