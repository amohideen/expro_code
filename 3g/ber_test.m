clear;
clc;
close all;
clear global;
%rng default;
global RxSignal;
global t_period;
global sample_period;
global transmit_time;
global receiving_time;
global total_time;
global amplitude1;
global amplitude2;
global decision_boundary;
global symbol_period;
global resolution; 
global Ctime;
global Dtime;
global Cfrequency; % career frequency
global GB;

GB = 2;                             %guard band
amplitude1 = 1;
amplitude2 = 0;
decision_boundary = amplitude1/2;
t_period = 1;                  % in seconds
resolution = 100; %dt frequency
Cfrequency = 10; % career frequency
sample_period = t_period/resolution;    %100bps
symbol_period = t_period/resolution;
Ctime = sample_period:sample_period:t_period;
Dtime = sample_period:sample_period:t_period;
transmit_time = 0;
receiving_time = 0;
total_time = 0;
ber = [];
setNodes; % creates nodes and sets parameters


% Waveform Generation, Decoding and BER Curve Calculation
% This code illustrates how to use the waveform generation and
% decoding functions for different frequency bands and compares the
% corresponding BER curves.
%%
EcNo = -25:2.5:8;                % Ec/No range of BER curves
spc = 4;                            % samples per chip
%msgLen = 8*120;                     % length in bits
%message = randi([0 1], msgLen, 1);  % transmitted message

% Preallocate vectors to store BER results:
[berASK868] = deal(zeros(1, length(EcNo)));

for idx = 1:length(EcNo) % loop over the EcNo range
  % ASK PHY, 868 MHz
  %waveform = lrwpan.PHYGeneratorASK(message, spc, '868 MHz');

  message = CreateMessage(62, 62, 0, 0)';  %CreateMessage(link_id, src_dst, data_type, load_type )
  Signal_Bits = DoubleCRC (message);       % along creates the CRC and appends to the message
  preSymbol = Signal_Bits';  
  symbols = DPIMSymbols2(preSymbol, GB);    %breaks the signal into 4 bit 14 blocks 
  shaped_pulse = PulseShaping(symbols);
  waveform = ModulatedSignal(amplitude1, amplitude2, symbols);
   
  K = 1;      % information bits per symbol
  SNR = EcNo(idx) - 10*log10(spc) + 10*log10(K);
  receiveds = awgn(waveform, SNR);
  


  [RxSignal Traps TrapsTotal] = node_2.received(receiveds, symbols);
 
  %bits     = lrwpan.PHYDecoderASK(received,  spc, '868 MHz');
  

    [noe berr] = biterr(symbols, RxSignal);
    ber = [ber berr];
  %[~, berASK868(idx)] = biterr(symbols, RxSignal(1:symbols));
  
end

% plot BER curve
figure;
semilogy(EcNo, ber, '-s');
legend('DPIM','Location', 'southwest')
title('BER Curves')
xlabel('Eb/No')
ylabel('BER')
%axis([min(EcNo) max(EcNo) 10^-2 1])
grid on
