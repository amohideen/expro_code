% function Tx = transmit(SNR, signal_tx)
% if nargin < 1
     SNR = 0;
% end    

freqs = 100;
Frame = 2048;
EbNo = 10;
tx_signal = Bask_Signal';
SNR = EbNo+10*log10(2);
AWGN = comm.AWGNChannel('EbNo', SNR);
ConstScope = comm.ConstellationDiagram;
Modulator = comm.QPSKModulator('BitInput',true);

hsb = dsp.SpectrumAnalyzer('SampleRate', freqs);
hts = dsp.TimeScope('SampleRate',freqs,'TimeSpan',4);

for i = 1:1:length(Bask_Signal)
   u = randi([0,1], Frame, 1);
   mod_signal = step(Modulator, u);
   pause(0.01);
   rx_signal = step (AWGN,tx_signal);
   
   step(ConstScope, rx_signal);
   
   step (hsb,rx_signal);
   
   step (hts,rx_signal);
end
release(AWGN);
release(ConstScope);
close all;


%end