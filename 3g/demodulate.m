for ii = 1:length(noisy_signal)
    if (noisy_signal(ii) > 0)
        rectified(ii) = noisy_signal(ii);
    else
        rectified(ii) = 0;
    end
end


b = fir1(1000, 0.01);

lp_demodulated = filter(b,1,rectified);

plot (lp_demodulated);