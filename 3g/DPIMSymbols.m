function DPIM = DPIMSymbols(signalbits, gbs)
SignalBits = signalbits;
GBS = gbs; %default number of guard band slots
DPIM=[];

msg_length = length(SignalBits);
num_symbols = ceil(msg_length/4);
% see if the message is of 4 bits else do padding (this should be done at mesage formation)
if  (msg_length/4 == 14)

    for symbol_count = num_symbols : -1 : 1 % high val: step: low val  
        symbol_matrix(symbol_count,:)= SignalBits(msg_length-3 : msg_length); % 14x4 binary blocks
        symbol_decimal(symbol_count,:) = bi2de(symbol_matrix(symbol_count,:),'left-msb'); % 14x1 bin to decimal
        
        temp_hex = num2str(symbol_matrix(symbol_count,:)); % turn a block of 4 bits from s_m into a binary string
        temp_hex(isspace(temp_hex)) = ''; % remove the gaps in the string
        symbol_hex = bi2hex(temp_hex); %turn the binary string to a hex symbol
         
        gray = hex_grey(symbol_hex); % find the gray_hex for each hex
        gray_coded_hex(symbol_count,:) = gray; % add the gray_hex to vector 
        num_gaps = grey_gap(gray); % find the gap multiplicator for each gray_hex
        
        % inserting number of zeros in DPIM
        temp=[zeros(1,(num_gaps+GBS))];
        % inserting '1' at the start of each symbol
        DPIM =[DPIM 1 temp];
        %DPIM =[1 temp DPIM];
        % disp equivalent values
%       disp (symbol_hex);
%       disp (gray);
%       disp(num_gaps);
%       disp('---');
%       disp (temp)
        msg_length = msg_length - 4;
    end
    else
    disp('Message is not in multiples of 4bits');
end

DPIM = [DPIM 1]; % last pulsesubplot(3,1,1);
 subplot(5,1,1);
 plot(DPIM);
 title('Binary Symbols');

