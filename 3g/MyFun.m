function [y1,y2]=MyFun(x1,x2)
% input argument x1 and x2 are not used in this example
y1=rand(x1,x2);
y2=x1:x2;