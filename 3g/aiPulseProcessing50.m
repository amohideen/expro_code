%
clc


%
clear BerTxSymbolsR;
clear OutBaskSignalR;
clear OutNoisySignalR;
clear BerTxSymbols_R;
clear cut;
clear cut2;
clear NoisySignalRow;
clear OutBaskSignal_R;
clear OutNoisySignal_R;
clear SignalRow;
clear c100;
noisyPulses(any(isnan(noisyPulses), 2), :)= [];
%

INPUT = [];
ZERO = [];
LABEL1 =[];
LABEL0 =[];
SIGNAL =[];

[n, m] = size(noisyPulses);
% Process the raw data
for row= 1:n
    c1 = 1;
    c50=75; 
    RoW = noisyPulses(row,:); % get each row of noisy pulse
    zero = ZeroNoises(row,:);
    signal = signalPulse(row,:);
    disp(row);
    
        for count = 1:1:3
                
                        cut = RoW(c1:c50);
                        cut2 = zero(c1:c50);
                        INPUT = [INPUT; cut];
                        LABEL1 = [LABEL1; 1]
                        ZERO = [ZERO; cut2];
                        LABEL0 = [LABEL0; 0];
                        SIGNAL = [SIGNAL; signal];
                        
                        c1 = c1+10;
                        c50 = c50 +10;
                disp (count);
        end
    
end

%pulse values
input = INPUT(1:3000, :);
INPUT_ALL = [INPUT_ALL; input];
target1 = LABEL1 (1:3000, :);
TARGET_ALL = [TARGET_ALL; target1];

%non-pulse
zeros = ZERO(1:3000, :);
INPUT_ALL = [INPUT_ALL; zeros];
target0 = LABEL0(1:3000, :);
TARGET_ALL = [TARGET_ALL; target0];





%save('Data/0dBINPUT');
