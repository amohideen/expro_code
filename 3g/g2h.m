function hex_code = g2h(gray)
switch gray
    case {0}
        hex_code = 0;
    case {1}
        hex_code = 1;
    case {2}
        hex_code = 3;
    case {3}
        hex_code = 2;
    case {4}
        hex_code = 7;
    case {5}
        hex_code = 6;
    case {6}
        hex_code = 4;
    case {7}
        hex_code = 5;
    case {8}
        hex_code = 15;
    case {9}
        hex_code = 14;
    case {10}
        hex_code = 12;
    case {11}
        hex_code = 13;
    case {12}
        hex_code = 8;
    case {13}
        hex_code = 9;
    case {14}
        hex_code = 11;
    case {15}
        hex_code = 10;
        
    otherwise
        hex_code = 0;
end