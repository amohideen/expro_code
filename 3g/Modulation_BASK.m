function[v]=Modulation_BASK(F1,F2,A)

% Modulation_BASK: is one of the digital modulation techniques in which the amplitude of carrier is 
%                  switched according to the binary data 
% 
% function arguments: 
% input: F1 the frequency of carrier 
%        F2 the frequency of message signal 
%        A the amplitude of the signal 
% output: v the modulated signal 
% 
% Example: Modulation_BASK(8,4,3);
% 
% Email: alikamal1@yahoo.com

t=0:0.001:1;            %Sampling Interval time 0 ? t ? 1 
x=A.*sin(2*pi*F1*t);    %Carrier Sine wave A sin(2?fct) fc:frequency of carrier 
u=A/2.*square(2*pi*F2*t)+(A/2); %Square wave message with peak of amplitude A and and peak of zero (binary data) 
v=x.*u;      %Modulation Process by multiply message with carrier v(t) = A u(t) sin 2?fct 
%Plot Carrier Signal 
figure('name','BASK MODULATION','numbertitle','off'); 
subplot(3,1,1); 
plot(t,x,'c','linewidth',2); 
xlabel('Time'); 
ylabel('Amplitude'); 
title('Carrier'); 
grid on; 
%Plot Message Singal 
subplot(3,1,2); 
plot(t,u,'r','linewidth',2); 
xlabel('Time'); 
ylabel('Amplitude'); 
title('Square Pulses'); 
axis([0 1 0 4]); 
grid on; 
%Plot Modulated Signal 
subplot(3,1,3); 
plot(t,v,'linewidth',2); 
xlabel('Time'); 
ylabel('Amplitude'); 
title('BASK Signal'); 
legend('BASKSIGNAL'); 
grid on; 
end