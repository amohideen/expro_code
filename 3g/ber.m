EcNo = -25:2.5:20;                % Ec/No range of BER curves
[BerBASK] = deal(zeros(1, length(EcNo))); % BerBASK array of length EcNo
spc = 1;

for indx = 1:length(EcNo) % loop over the EcNo range
    
  waveform = Txpulse;
  K = 1;      % information bits per symbol
  SNR = EcNo(indx) - 10*log10(spc) + 10*log10(K);
  received = awgn(waveform, SNR);
  signal_mn = bercal(received);
  bits = signal_mn;

  %  [~, BerBASK(indx)] = biterr(waveform, bits(1:msgLen));
%  [~, BerBASK(indx)] = biterr(waveform, bits);
   [~, BerBASK(indx)] = biterr(Txpulse, bits);
  
end

% plot BER curve
semilogy(EcNo, BerBASK,    '-s')
%legend('ASK, 868 MHz', 'Location', 'southwest')
title('2G EXpro BER Curve')
xlabel('Bit Energy to Noise Spectral Density, Ec/No (dB)')
ylabel('BER')
axis([min(EcNo) max(EcNo) 10^-2 1])
grid on



