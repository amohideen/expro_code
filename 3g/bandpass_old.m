function fSignal = bandpass(signal)
bit_rate = 100;
filter_order = 1;
cut_off = [5 6]*2/bit_rate;
[b a] = butter(filter_order, cut_off, 'bandpass');
fSignal = filter(b,a, signal);
end
 