% transform the Signalbit [56x1] array to [1x1] string array 
temp_str = num2str(SignalBits');
% remove the space between the bits in the 1x1 string array 
temp_str(isspace(temp_str)) = '';
%length of the string array
msg_length = length(temp_str);
%disp(i);
% number of symbols in a total message at 4 bit/symbol 
symbols = ceil(msg_length/4); 
TxSignal =[];
% for function to traverse the message bits
for g = symbols : -1 : 1 % high val: step: low val  
    if msg_length > 4
       %disp(g);
       % calculate the Hex val of each 4 bit from MSB
       hex_str(g) = binary_hex(temp_str(msg_length-3 : msg_length));
       disp ('Hex:');
       disp (hex_str(g));
       
       grey_str(g) = hex_grey(hex_str(g));
       disp ('Grey:');
       disp (grey_str(g));
       
       %bin_str(g) = hex_bin(grey_str(g));
       disp ('Binary:');
       disp (hex_bin(grey_str(g)));
       grey_bin = (hex_bin(grey_str(g)'));
       temp_grey_bin_signal = grey_bin';
       TxSignal = [TxSignal; temp_grey_bin_signal];
       msg_length = msg_length - 4;
    else
        hex_str(g) = binary_hex(temp_str(1 : msg_length));
        grey_str(g) = hex_grey(hex_str(1 : g));
        TxSignal = [TxSignal; temp_grey_bin_signal];
    end
end
disp (hex_str);
disp (grey_str);
pulse = dpims(4,14,2);
