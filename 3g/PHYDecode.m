       %% implement the demodulation in here 
        function DPIMDU = PHYDecode(signal,period)
            
            BASK_Signal = signal;
            bit_period = period;
            %DPIM = dpim;
            bit_rate=1/bit_period;                                                         % bit rate
            career_frequency=bit_rate*50; %(10MHz)    

           %XXXXXXXXXXXXXXXXXX Binary ASK demodulation XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
            DPIMDU=[];
            
            time_2=bit_period/99:bit_period/99:bit_period;
            ss=length(time_2);
            
            for num=ss:ss:length(BASK_Signal) %% for the length of bask signal
                t=bit_period/99:bit_period/99:bit_period;
                career=cos(2*pi*career_frequency*t); % carrier signal 
                mm=career.*BASK_Signal((num-(ss-1)):num); %%%%%%%%%%%%%%%%%%%%%%
            
                time_4=bit_period/99:bit_period/99:bit_period;
                z=trapz(time_4,mm);                                              % intregation 
                zz=round((2*z/bit_period));                                     
                if(zz>5)                                  % logic level = (A1+A2)/2=7.5
                    a=1;
                else
                    a=0;
                end
                DPIMDU=[DPIMDU a];
            end
        
        end