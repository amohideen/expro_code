function Tx = transmit(snr, tx_signal)
close all;

SNR = snr;
freqs = 100;
EbNo = 10;
signal_tx = tx_signal;

%SNR = EbNo+10*log10(2);
AWGN = comm.AWGNChannel('EbNo', SNR); %AWGN channel
ConstScope = comm.ConstellationDiagram; %constellation
SpectrumScope = dsp.SpectrumAnalyzer('SampleRate', freqs);
TimeScope = dsp.TimeScope('SampleRate',freqs,'TimeSpan',100);

for tx = 1:1:length(signal_tx)
   
   pause(0.0100);
   rx_signal = step(AWGN,signal_tx(tx));
   
   %step(ConstScope, rx_signal, tx_time);
   
   step (SpectrumScope,rx_signal);
   
   step (TimeScope,rx_signal);
   
end
release(AWGN);
release(ConstScope);


 end