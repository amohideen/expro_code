% function symbols = symbols(signalbits)
 SignalBits = Signal_Bits;
%clear;
close all;
%main;
%T=1;
GBS=2; %default number of guard band slots
DPIM=[];

msg_length = length(SignalBits);
num_symbols = ceil(msg_length/4);
% see if the message is of 4 bits else do padding (this should be done at mesage formation)
if  (msg_length/4 == 14)

    for symbol_count = num_symbols : -1 : 1 % high val: step: low val  
        symbol_matrix(symbol_count,:)= SignalBits(msg_length-3 : msg_length); % 14x4 binary blocks
        symbol_decimal(symbol_count,:) = bi2de(symbol_matrix(symbol_count,:),'left-msb'); % 14x1 bin to decimal
        
        temp_hex = num2str(symbol_matrix(symbol_count,:)); % turn a block of 4 bits from s_m into a binary string
        temp_hex(isspace(temp_hex)) = ''; % remove the gaps in the string
        symbol_hex = bi2hex(temp_hex); %turn the binary string to a hex symbol
         
        gray = hex_grey(symbol_hex); % find the gray_hex for each hex
        gray_coded_hex(symbol_count,:) = gray; % add the gray_hex to vector 
        num_gaps = grey_gap(gray); % find the gap multiplicator for each gray_hex
        
        % inserting number of zeros in DPIM
        temp=[zeros(1,(num_gaps+GBS))];
        % inserting '1' at the start of each symbol
        DPIM =[DPIM 1 temp];
       
        % disp equivalent values
        disp (symbol_hex);
        disp (gray);
        disp(num_gaps);
        disp('---');
        disp (temp)
                
        msg_length = msg_length - 4;

    end
    else
    disp('Message is not in multiples of 4bits');
end

DPIM = [DPIM 1]; % last pulse
%plot(DPIM);
%stem(DPIM)
%-----------------------------------------------------

%% --------------------------------
amplitude1 = 5; % Amplitude for 1 bit
amplitude2 = 0; % Amplitude for 0 bit
career_frequency = 20; % Frequency of Modulating Signal [Career Signal]
sampling_frequency = 100; % Sampling rate - This will define the resoultion/must match the channel for timing
bit_time = 0: 1/sampling_frequency : 1; % Time window for one bit sampling
bit_rate = 1/sampling_frequency;
time = []; % This time variable is just for plot

Bask_Signal = []; % Binary Amplitude Shift Keying
Digital_Signal = []; % Timed gray code

for bit = 1: 1: length(DPIM)
    
    % The BASK Signal
    Bask_Signal = [Bask_Signal (DPIM(bit)==1)*amplitude1*sin(2*pi*career_frequency*bit_time)+...
        (DPIM(bit)==0)*amplitude2*sin(2*pi*career_frequency*bit_time)];
    
    % The Original Digital Signal
    Digital_Signal = [Digital_Signal (DPIM(bit)==0)*...
        zeros(1,length(bit_time)) + (DPIM(bit)==1)*ones(1,length(bit_time))];
    
    time = [time bit_time];
    bit_time =  bit_time + 1; 
end

% % Plot the ASK Signal
% subplot(2,1,1);
% plot(time,BASK_signal,'LineWidth',2);
% xlabel('Time (bit period)');
% ylabel('Amplitude');
% title('BASK Career modulated Signal');
% %axis([0 time(end) 1.5 1.5]);
% grid  on;
% 
% % Plot the Original Digital Signal
% subplot(2,1,2);
% plot(time,Digital_signal,'r','LineWidth',2);
% xlabel('Time (bit period)');
% ylabel('Amplitude');
% title('Gray coded Digital Signal');
% axis([0 time(end) -0.5 1.5]);
% grid on;
%symbols = true;
%end
