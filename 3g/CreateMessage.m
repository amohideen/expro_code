function message = CreateMessage(link_id, src_adrs,dst_adrs, data_type, load_type )

%% Message
message = twog;
message.LinkID = link_id;
message.SrcAdr = src_adrs;
message.DstAdr = dst_adrs;
message.PayloadType = data_type;
message.DataType = load_type;
message.Data=randi([67000000 67108863],1,1);
message.Padding=[];
%message.gcrc=[0 0 0 0 0 0 0]';
%% CRC creation and appending to the header+paylaod

MessageBits=[message.LinkID; ...
           message.SrcAdr; ...
           message.DstAdr; ...
           message.PayloadType; ...
           message.DataType; ...
           message.Data; ...
           message.Padding];
           %message.gcrc];
       
% crcGen = comm.CRCGenerator('x^7+ x^4+ x^3+ x^1+ x^0');
% reset(crcGen); 
% message = crcGen(MessageBits);
 message = MessageBits;

            
%message = MessageBits;
end

%code = encode(msg,n,k,'hamming/binary')
%decData = decode(encData,n,k,'hamming/binary');
%numerr = biterr(data,decData)


