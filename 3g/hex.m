temp_str = num2str(SignalBits');
temp_str(isspace(temp_str)) = '';
i = length(temp_str);
disp(i);
n = ceil(i/4); 
TxSignal =[];

for g = n : -1 : 1
    if i > 4
       disp(g);     
       hex_str(g) = binary_hex(temp_str(i-3 : i));
       inpb(g,:)= logical(temp_str(i-3 : i));
       disp ('Hex:');
       disp (hex_str(g));
       
       grey_str(g) = hex_grey(hex_str(g));
       disp ('Grey:');
       disp (grey_str(g));
       
       %bin_str(g) = hex_bin(grey_str(g));
       disp ('Binary:');
       disp (hex_bin(grey_str(g)));
       grey_bin = (hex_bin(grey_str(g)'));
       temp_grey_bin_signal = grey_bin';
       TxSignal = [TxSignal; temp_grey_bin_signal];
       i = i - 4;
    else
        hex_str(g) = binary_hex(temp_str(1 : i));
        grey_str(g) = hex_grey(hex_str(1 : g));
        TxSignal = [TxSignal; temp_grey_bin_signal];
    end
end
disp (hex_str);
disp (grey_str);
pulse = dpims(4,14,2);
