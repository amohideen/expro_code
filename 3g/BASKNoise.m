close all
clc
%% BPSK modulatde signal
% Digital_Signal = expanded DPIM signal 
% communication systems use i and q channels
% BPSK uses only one channel
% Below are samples of creating binary signals
%signal_i = 2*(rand(1,10^5)>0.5)-1; % creates BPSK 1s and -1s

signal_temp = Digital_Signal; % creates BPSK 1s and -1s
signal_q = zeros(1,length(Digital_Signal));
signal_i = [];

for tmp=1:1:length(signal_temp)
        if signal_temp(tmp)== 1
            a = 1;
        else
           a = -1;
        end
    signal_i = [signal_i a];
end

signal_c = complex(signal_i,signal_q);

% scatter plot to show I (x-axiz) and Q (y-axiz) channels
scatter(signal_i, signal_q);

% noise can be added in 3 ways. 1-variance, 2-SNR 3-No value
% creting noise on known variance
% signal is complex, therefore, noise should be comples too 
% noise needs to be uncorrelated, 0 meas and orthogonal, independent

EbNo = 1/50:1/10:1;     
[EbNoBASK] = deal(zeros(1, length(EbNo)));

    
for index = 1:length(EbNo)
    % this noise is the random noise with certain power irrespective of 
    % signal power
    %noice = 1/sqrt(2)*(randn(1,10^5)+i*randn(1,10^5))*var;
    noice = 1/sqrt(2)*(randn(1,length(signal_i))+i*randn(1,length(signal_i)))*EbNo(index);
    addNoise = signal_c+noice;
    
   %  [~, EbNoBASK(index)] = real(addNoise);
     %imag(addNoise)
end


%% Noise with SNR

% to see signal power
p_signal = mean(abs(signal_c).^2);

% signal energy or energy/bit
e_signal = (abs(signal_c).^2);


for snr = 0:1:10 %dB
    % snr linear
    snr_lin = 10^(snr/10); %Signal/Noise = SNR-Linear
    ebno_lin = 10^((snr-3)/10);
    
    index = (p_signal/snr_lin);
    var_ebno = (p_signal/ebno_lin);
    
    %noise based on SNR
%    noice = 1/sqrt(2)*(randn(1,10^5)+i*randn(1,10^5))*var;
    %noise based on EbNo
    noice = 1/sqrt(2)*(randn(1,length(signal_i)))+i*randn(1,length(signal_i))*var_ebno;
    addNoise = signal_c+noice;
    
    figure(2);
    plot (real(addNoise),imag(addNoise), '*');
    drawnow("expose");
end