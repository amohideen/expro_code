%clear all
close all
clc

%% BPSK modulatde signal
% communication systems use i and q channels
        % BPSK uses only one channel
        % Below are samples of creating binary signals
        %base_signal = rand(1,10^5)>0.5; % creates 1s and 0s
        %base_signal = (rand(1,10^5)>0.5)-1;
%signal_i = 2*(rand(1,10^5)>0.5)-1; % creates BPSK 1s and -1s
signal_i = shaped_pulse;
        %signal_i = de2bi(264069491);
        %signal_q = zeros(1,length(signal_i));
%signal_q = zeros(1,10^5);
signal_q = zeros(1,length(shaped_pulse));

signal = complex(signal_i,signal_q);

        % scatter plot to show I (x-axiz) and Q (y-axiz) channels
scatter(signal_i, signal_q);

    % noise can be added in 3 ways. 1-variance, 2-SNR 3-No value
    % creting noise on known variance
    % signal is complex, therefore, noise should be comples too 
    % noise needs to be uncorrelated, 0 meas and orthogonal, independent


for var = 1/50:1/10:0.5
    % this noise is the random noise with certain power irrespective of 
    % signal power
    %noice = 1/sqrt(2)*(randn(1,10^5)+i*randn(1,10^5))*var;
    noice = 1/sqrt(2)*(randn(1,length(signal_i))+i*randn(1,length(signal_i)))*var;
    addNoise = signal+noice;
    
    figure(1);
    plot (real(addNoise),imag(addNoise), '*');
    drawnow("expose");
    pause(1);
end


%% Noise with SNR

% to see signal power
p_signal = mean(abs(signal).^2);

% signal energy or energy/bit
e_signal = (abs(signal).^2);

for snr = 0:1:10 %dB
    % snr linear
    snr_lin = 10^(snr/10); %Signal/Noise = SNR-Linear
    var = (p_signal/snr_lin);
    
    %noise based on SNR
    noice = 1/sqrt(2)*(randn(1,length(signal_i))+i*randn(1,length(signal_i)))*var;
    %noise based on EbNo
    addNoise = signal+noice;
    
    figure(2);
    plot (real(addNoise),imag(addNoise), '*');
    drawnow("expose");
     pause(1);
end


%% Noise with EbNo
for snr = 0:1:10 %dB
    % snr linear
    snr_lin = 10^(snr/10); %Signal/Noise = SNR-Linear
    ebno_lin = 10^((snr-3)/10);
    
    var = (p_signal/snr_lin);
    var_ebno = (p_signal/ebno_lin);
    
    %noise based on SNR
%    noice = 1/sqrt(2)*(randn(1,10^5)+i*randn(1,10^5))*var;
    %noise based on EbNo
    noice = 1/sqrt(2)*(randn(1,length(signal_i))+i*randn(1,length(signal_i)))*var_ebno;
    addNoise = signal+noice;
    
    figure(3);
    plot (real(addNoise),imag(addNoise), '*');
    drawnow("expose");
     pause(1);
end


