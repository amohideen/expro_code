function hex = bi2hex(binary)
switch binary
    case {'0', '00', '000', '0000'}
        hex = '0';
    case {'1', '01', '001', '0001'}
        hex = '1';
    case {'10', '010', '0010'}
        hex = '2';
    case {'11', '011', '0011'}
        hex = '3';
    case {'100', '0100'}
        hex = '4';
    case {'101', '0101'}
        hex = '5';
    case {'110', '0110'}
        hex = '6';
    case {'111', '0111'}
        hex = '7';
    case '1000'
        hex = '8';
    case '1001'
        hex = '9';
    case '1010'
        hex = 'A';
    case '1011'
        hex = 'B';
    case '1100'
        hex = 'C';
    case '1101'
        hex = 'D';
    case '1110'
        hex = 'E';
    case '1111'
        hex = 'F';
end

 