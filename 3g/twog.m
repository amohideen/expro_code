classdef twog
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        LinkID      = zeros(4,1);   % 4 binary bits
        SrcAdr      = zeros(4,1);   % 4 binary bits
        DstAdr      = zeros(4,1);   % 4 binary bits
        PayloadType = zeros(2,1);   % 2 binary bits    
        DataType    = zeros(2,1) ;  % 2 binary bits
        Data        = zeros(28,1);  % 28 binary bits (44 bits)
        Padding     = zeros(1,1); % 2 binary bits (52)
       % gcrc        = zeros(7,1);
                   % including 7 bit of CRC, the total length of packet is 56 bits
            
    end
    %%
    methods
        %%   set the LinkID
        function frame = set.LinkID(frame,lnkid)
            %UNTITLED Construct an instance of this class
            

            if (lnkid == 15 || lnkid<15)
                frame.LinkID = de2bi(lnkid, 4, 'left-msb')';
                link = de2bi(lnkid, 4, 'left-msb');
                %disp('Address set');
                fprintf('Message: Link ID is set to : \n');
                disp(link);
            else
                disp('The value MUST be 15 or below 15');
            end
        end
        
        %% set the SrcAdr
        
        function frame = set.SrcAdr(frame,srcdst)
            %UNTITLED Construct an instance of this class
            %   set the source or destination address

            if (srcdst == 15 || srcdst<15)
                frame.SrcAdr = de2bi(srcdst, 4, 'left-msb')';
                srcAddress = de2bi(srcdst, 4, 'left-msb');
                %disp('Address set');
                fprintf('Message: Source Address is set to : %s \n', transpose(srcAddress));
                disp(srcAddress);
            else
                disp('The value MUST be 15 or below 15');
            end
        end
        
         %% set the DstAdr
        
        function frame = set.DstAdr(frame,dstadr)
            %UNTITLED Construct an instance of this class
            %   set the source or destination address

            if (dstadr == 15 || dstadr<15)
                frame.DstAdr = de2bi(dstadr, 4, 'left-msb')';
                dstAddress = de2bi(dstadr, 4, 'left-msb');
                %disp('Address set');
                fprintf('Message: Destination Address is set to : %s \n', transpose(dstAddress));
                disp(dstAddress);
            else
                disp('The value MUST be 15 or below 15');
            end
        end
      
        %% set PayloadType
        function frame = set.PayloadType(frame,pldt)
            %UNTITLED Construct an instance of this class
            %   set the PayloadType
            %sch_pressure = [0 0]'  0 – scheduled pressure/temperature message
            %odm_pressure = [0 1]'  1 – on demand pressure/temperature message
            %config = [1 0]'       2 – configuration change message e.g. schedule change, ping, pwm
            %history = [1 1]'      3 – historic data request message

            if (pldt == 0)
                frame.PayloadType = [0 0]';
                disp('Message: The payload set to scheduled pressure/temperature message');
                
            elseif(pldt ==1)
                frame.PayloadType = [0 1]';
                disp('Message: The payload set to on demand pressure/temperature message');
            
            elseif(pldt ==2)
                frame.PayloadType = [1 0]';
                disp('Message: The payload set to configuration change message');
                
            elseif(pldt ==3)
                frame.PayloadType = [1 1]';
                disp('Message: The payload set to historic data request message');
            else
                disp ('Invalid input');
            end
             
         end
        
        %% set DataType
        function frame = set.DataType(frame,dttp)
            %UNTITLED Construct an instance of this class
            %   set the DataType

             if (dttp == 0)
                frame.DataType = [0 0]';
                fprintf('\n');
                disp('Message: The Data type is set to type-1');
                
            elseif(dttp ==1)
                frame.DataType = [0 1]';
                fprintf('\n');
                disp('Message: The Data type is set to type-2');
            
            elseif(dttp ==2)
                frame.DataType = [1 0]';
                fprintf('\n');
                disp('Message: The Data type is set to type-3');
                
            elseif(dttp ==3)
                frame.DataType = [1 1]';
                fprintf('\n');                
                disp('Message: The Data type is set to type-4');
            else
                disp ('Invalid input');
            end
           end
        
        %% set Data
        function frame = set.Data(frame,data)
            
            if (data == 268435455 || data<268435455) %32 binary 1s equivalent (only for testing)
                frame.Data = de2bi(data, 28, 'left-msb')';
                %data_ = de2bi(data);
                %disp('Data set');
                fprintf('\n');
                fprintf('Message: Data is set to the Binary representation of: %d \n', data);
                %disp(data_);
            else
                disp('The value MUST be 7 or below 4294967295');
            end
        end
        
    end
end

