%clear;
clc
close all;

%clear global;
%rng default;
global RxSymbols;
global period;
global transmit_time;
global receiving_time;
global total_time;
global amplitude1;
global amplitude2;
global decision_boundary;
global symbol_samples;
global Fresolution;
global Frequency;
global Ctime;
global Dtime;
global Tsymbols; % Symbol period 
global GB;
global miniBatchSize;

miniBatchSize = 51;
GB = 2; %guard band
period = 1; % in seconds
amplitude1 = 1;
amplitude2 = 0;
decision_boundary = amplitude1/2;

Fresolution = 100; % samples in a cycle
Frequency = period/Fresolution;    %0.001 normalised frequency
Tsymbols = 1; % number of cycles ber t period /10 cycles in 1 second
symbol_samples = Tsymbols*Fresolution;

Ctime = Frequency:Frequency:period;
Dtime = Frequency:Frequency:period;

transmit_time = 0;
receiving_time = 0;
total_time = 0;
%SnR = 0;
setNodes; % creates nodes and sets parameters
%% Message
message = CreateMessage(1, 1, 2, 1, 1)';  %CreateMessage(link_id, src_adrs,dst_adrs, data_type, load_type)
%dlmwrite('OutMessageBits_R.dat', message, '-append','delimiter', '\t');
%write 1

signal_bits = message;
%SnR= -10;
snr_noise = SnR;

% %% Initiate transmission
TxSymbols = DPIMSymbols2(signal_bits, GB); %breaks the signal into 4 bit 14 blocks 
%dlmwrite('BerTxSymbols_R.dat', TxSymbols, '-append','delimiter', '\t');
%write 2
% Txpulse = PulseShaping(symbols);
RLE_1 = diff([0 find(diff(TxSymbols)~=0) length(TxSymbols)]); %count of 0s between 2 1s

bask_signal = ModulatedSignal(amplitude1, amplitude2, TxSymbols);
%dlmwrite('OutBaskSignal_R.dat', bask_signal, '-append','delimiter', '\t'); 
%bask_signal = complex (bask_signal);
%dlmwrite('BerBaskSignal.dat', bask_signal, '-append','newline','pc','delimiter', '\t');
% write 3

%% Perform transmission
node_state = 1; 

%    %snr_noise = -20;
% %   p_signal = mean(abs(bask_signal).^2); % to see signal power
% %   e_signal = (abs(bask_signal).^2); % signal energy or energy/bit
% %   snr_lin = 10^(snr_noise/10); %Signal/Noise = SNR-Linear
% %   var = (p_signal/snr_lin);
% %   noice = 1/sqrt(2)*(randn(1,length(bask_signal))+i*randn(1,length(bask_signal)))*var;  %noise based on SNR
% %   %noise based on EbNo
% %   noisy_signal = bask_signal + noice;

noisy_signal = awgn((bask_signal), snr_noise);
%dlmwrite('OutNoisySignal_R.dat', noisy_signal, '-append','delimiter', '\t'); 

%dlmwrite('BerNoisySignal.dat', noisy_signal, '-append','newline','pc','delimiter', '\t');
%write 4
     % %noisy_signal = bask_signal;
transmitted_signal = node_1.transmits(node_state, noisy_signal, TxSymbols);

% %noisy signal_plot;
[RxSymbols, RxMessage, Traps, TrapsTotal] = node_2.receivedai(noisy_signal, TxSymbols);
%[RxSymbols, Traps, TrapsTotal] = node_2.received(noisy_signal, TxSymbols);

%dlmwrite('BerRxSymbols.dat', RxSymbols, '-append','newline','pc','delimiter', '\t');
% write 5
          
          % check if the message is for me
          binary_signal = decrypt(node_2, RxSymbols);
          SrcDst = GetAddress(node_2, binary_signal);
          ReTx = node_2.IsForMe(SrcDst);

% dlmwrite('TxTime.dat', transmit_time, '-append','delimiter', '\t');
% dlmwrite('RxTime.dat', receiving_time, '-append','delimiter', '\t');
% dlmwrite('TRxTime.dat', total_time, '-append','delimiter', '\t');
% write 6 times 



TransmitTime = [TransmitTime transmit_time]; %40mW + 100W
ReceiveTime = [ReceiveTime receiving_time]; %40mW
TexLength = [TexLength length(TxSymbols)];


Ppulses = NoPulse*Pt*Tperiod; 
PsetupDelay = SetupTime*Pidle; 
Pamp = NoPulse*Tperiod*Pidle;
PsynthTx = transmit_time*Pidle;

Etx = (Ppulses + PsetupDelay + Pamp + PsynthTx);
TxEnergy = [TxEnergy Etx];
%display(Etx)

ProcessTime =7;
PprocessDelay = ProcessTime *Pidle;
PsynthRx = receiving_time * Pidle;
Perror = 11* Pidle; % 11 blocks
Erx = (PprocessDelay + PsynthRx + Perror);
RxEnergy = [RxEnergy Erx];
%display(Erx)

Energy = [Energy (Etx+Erx)];

%simulated BER computation
 ber = [];
 [noe berr] = biterr(TxSymbols, RxSymbols);
 %[noe berr] = biterr(message, RxMessage);
 ber = [ber berr];
 %dlmwrite('BerVal_R.dat', ber, '-append','delimiter', '\t');
 try
 numerr = biterr(message,RxMessage);
 %dlmwrite('BerNumError.dat', numerr, '-append','delimiter', '\t'); 
 catch
 end
