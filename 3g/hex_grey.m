function grey_code = hex_grey(hexval)
switch hexval
    case {'0'}
        grey_code = '0';
    case {'1'}
        grey_code = '1';
    case {'2'}
        grey_code = '3';
    case {'3'}
        grey_code = '2';
    case {'4'}
        grey_code = '6';
    case {'5'}
        grey_code = '7';
    case {'6'}
        grey_code = '5';
    case {'7'}
        grey_code = '4';
    case '8'
        grey_code = 'C';
    case '9'
        grey_code = 'D';
    case 'A'
        grey_code = 'F';
    case 'B'
        grey_code = 'E';
    case 'C'
        grey_code = 'A';
    case 'D'
        grey_code = 'B';
    case 'E'
        grey_code = '9';
    case 'F'
        grey_code = '8';
end

 